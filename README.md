# Funkce #
Oskenuje složku *local_path* a všechny soubory modifikované za posledních *beckup_period* dnů odešle na FTP server do složky *remote_path*/*aktualni_datum*. Pokud je povoleno mazání starých souborů (*file_remove_period != None*) smaže všechny soubory starší než *file_remove_period* dnů.

Script zasílá emailem informace o úspěšném a neúspěšném nahrání souboru. Informace ukládá také do logu *log_path*.