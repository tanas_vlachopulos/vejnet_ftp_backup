# -*- coding: utf-8 -*-
import ftplib
import os
import smtplib
from ftplib import FTP
import datetime

############## CONFIG ###############

# backup period in days
backup_period = 50

# remove files from local directory older then given days
# turn off removal function: file_remove_period = None
file_remove_period = None

# remote ftp host
ftp_host = "admin.netbackup.cz"

# ftp user name
ftp_user = "j.kovac@vejnet.cz"

# ftp password
ftp_password = "vejvar7X7"

# local directory with files for backup
# !!! paths must not end with a slash !!!
local_path = "c:/Users/Tanas/pracovni/test_script"

# remote directory with backups
# !!! paths must not end with a slash !!!
remote_path = 'pohoda'

# e-mail recipient crash reports
notification_email = 'it@vejnet.cz'

# SMTP server
smtp_host = 'smtp.vejnet.cz'

# log path
log_path = "c:/Users/Tanas/pracovni/ftp_log.txt"

############# END CONFIG ################


def logger(message):
    """
    Log message to file and to console
    """
    with open(log_path, 'a') as log_file:
        now = datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
        log_file.write(now + ' >>> ' + message + '\n')
        print(now + ' >>> ' + message)


def emailer(subject, message):
    """
    Send email via SMTP server
    :param email subject:
    :param email message:
    """
    msg = "From: FTP Backup <backup@vejnet.cz> \n"
    msg += "To: {}\n".format(notification_email)
    msg += "MIME-Version: 1.0\n"
    msg += "Content-type: text/html\n"
    msg += "Subject: {} FTP Backup\n{}\n".format(subject, message)

    try:
        smtp = smtplib.SMTP(smtp_host)
        smtp.sendmail('backup@vejnet.cz', [notification_email], msg.encode('utf-8'))
        logger("{} e-mail send".format(subject))
    except smtplib.SMTPException:
        logger('Error unable to send e-mail')


def remove_old_file_from_local_dir(dir_path, older_then):
    # remove file from local directory older then given linux timestamp
    files_to_remove = [name for name in os.listdir(dir_path)
                      if os.path.isfile(dir_path + '/' + name) and os.path.getmtime(dir_path + '/' + name) < older_then]
    for name in files_to_remove:
        os.remove(dir_path + '/' + name)
        logger("File " + name + ' has been removed')


def get_file_names_from_local_dir(dir_path, newer_then=None):
    if newer_then:
        # return file names from local directory newer then given linux timestamp
        return [name for name in os.listdir(dir_path)
            if os.path.isfile(dir_path + '/' + name) and os.path.getmtime(dir_path + '/' + name) > newer_then]
    else:
        # return all file names from local directory
        return [name for name in os.listdir(dir_path) if os.path.isfile(dir_path + '/' + name)]


def get_file_names_from_ftp_server(ftp_instance, dir_path):
    ftp_instance.cwd(dir_path)  # open given directory
    files = ftp_instance.nlst()  # list file from given directory
    ftp_instance.cwd("/")
    return [name for name in files if name != '.' and name != '..']


def send_files_to_ftp(local_path, newer_then, ftp_instance, remote_directory):
    # get files for newer then given linux timestamp
    files_to_transfer = get_file_names_from_local_dir(local_path, newer_then)
    remote_files = get_file_names_from_ftp_server(ftp_instance, remote_directory)
    # files_to_transfer = [name for name in local_files if name not in remote_files]

    now = datetime.datetime.now().strftime("%d-%m-%Y")  # folder name
    ftp_instance.cwd(remote_directory)
    if now not in remote_files:  # handle folder name duplication
        ftp_instance.mkd(now)

    ftp_instance.cwd(now)
    error_files = []

    for name in files_to_transfer:
        with open(local_path + '/' + name, 'rb') as file:
            # file = open(local_path + '/' + name, 'rb')
            status = ftp.storbinary('STOR ' + name, file)
            if status == '226 Transfer complete':
                logger('File ' + name + ' has been successfully upload to FTP folder ' + now)
            else:
                logger('Some error was occurred during uploading file ' + name + ' to FTP server')
                error_files.append(name)

    ftp_instance.cwd('/')
    return error_files


ftp = None
try:
    ftp = FTP(host=ftp_host, user=ftp_user, passwd=ftp_password)

    time_now = datetime.datetime.now()
    backup_delta = datetime.timedelta(days=backup_period)

    # converted_time = datetime.datetime.strptime('06-10-2016', "%d-%m-%Y")

    logger('Files to backup: ' + str(get_file_names_from_local_dir(local_path, (time_now - backup_delta).timestamp())))
    error_files = send_files_to_ftp(local_path, (time_now - backup_delta).timestamp(), ftp, remote_path)
    if not error_files and file_remove_period:
        remove_delta = datetime.timedelta(days=file_remove_period)
        remove_old_file_from_local_dir(local_path, (time_now - remove_delta).timestamp())

    if error_files:
        logger('Error these files can not be uploaded to FTP: ' + str(error_files))
        emailer("Problem?!", "Následující soubory se nepodařilo nahrát na FTP server:\n{}".format(str(error_files)))
    else:
        success_files = get_file_names_from_local_dir(local_path, (time_now - backup_delta).timestamp())
        logger('Files to backup: ' + str(success_files))
        emailer('Uspech', "Znamenitě, pravidelná záloha proběhla úspěšně, následující soubory byly nahrány na FTP do složky {}:\n{}"
                .format(datetime.datetime.now().strftime("%d-%m-%Y"), str(success_files)))


except ftplib.all_errors as error:
    logger('Can not connect to FTP server')
    logger(str(error))
    emailer('Problem?!', 'Nepodařilo se připojit k FTP serveru z důvodu následující chyby:\n{}'.format(error))
except Exception as error:
    logger('Some error was occurred')
    logger(str(error))
    emailer('Problem?!', 'Úlohu zálohování se nepodařilo spusti z důvodu následující chyby:\n{}'.format(error))

finally:
    if ftp:
        ftp.quit()